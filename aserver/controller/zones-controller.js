'use strict';

var
  BaseController = require('./base-controller'),
  Zone = require('../model/zone'),
  ZoneCoordinate = require('../model/zone-coordinate');

class ZonesController extends BaseController {

  constructor () {
    super(BaseController);
  }

  static createZone (values) {
    return Zone.create(values);
  }

  static addCoordinates (zoneId, lat, lng) {
    return new Promise((resolve, reject) => {

      var coordinates = {
        lat : lat,
        lng : lng
      };

      Zone.findAll({
        where : {
          id : zoneId
        }
      })
        .then((zone) => {

          if (!zone[0]) {
            console.log(zone)
            reject(404);
            
          } else {

            ZoneCoordinate.create({
              lat : coordinates.lat,
              lng : coordinates.lng,
              zoneId : zoneId
            })
              .then(data => {
                resolve(data);
              })
              .catch(err => {
                reject(err);
              });
          }

        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  static getAll (criteria) {

    criteria = criteria ? criteria : {};

    return Zone.findAll({
      where : criteria
    });
  }

  static getById (id) {
    return Zone.findById(id);
  }

  static updateById (id, values) {
    return Zone.update(values, {
      where : {
        id
      }
    });
  }

  static deleteZone (id) {
    return Zone.destroy({
      where : {
        id
      }
    });
  }

}

module.exports = ZonesController;