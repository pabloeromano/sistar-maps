'use strict';

var
  BaseController = require('./base-controller'),
  Mobile = require('../model/mobile'),
  MobileCoordinate = require('../model/mobile-coordinate');

class MobilesController extends BaseController {

  constructor () {
    super(BaseController);
  }

  static createMobile (values) {
    return Mobile.create(values);
  }

  static addCoordinates (mobileId, lat, lng) {
    return new Promise((resolve, reject) => {

      var coordinates = {
        lat : lat,
        lng : lng
      };

      Mobile.findAll({
        where : {
          id : mobileId
        }
      })
        .then((mobile) => {

          if (!mobile[0]) {
            reject(404);
          } else {

            MobileCoordinate.create({
              lat : coordinates.lat,
              lng : coordinates.lng,
              mobileId : mobileId
            })
            .then(data => {
              resolve(data);
            })
            .catch(err => {
              reject(err);
            });
          }

        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  static getAll (criteria) {

    criteria = criteria ? criteria : {};

    return Mobile.findAll({
      where : criteria
    });
  }

  static getById (id) {
    return Mobile.findById(id);
  }

  static updateById (id, values) {
    return Mobile.update(values, {
      where : {
        id
      }
    });
  }

  static deleteMobile (id) {
    return Mobile.destroy({
      where : {
        id
      }
    });
  }

}

module.exports = MobilesController;