'use strict';

var
  fs = require('fs'),
  Sequelize = require('sequelize'),
  sequelize = null,
  dbConfig = null,
  config = null,
  db = null;

class DatabaseConnection {

  static getInstance () {
    if (db !== null) {
      return db;
    }
  }

  static connect (done) {

    function sequel (configuration, cb) {

      if (configuration.dialect === 'postgres') {
        dbConfig = 'postgres://' + configuration.user + ':' + configuration.password + '@'
          + configuration.host + '/' + configuration.dbName;
        sequelize = new Sequelize(dbConfig);
      } else if (configuration.dialect === 'mssql') {
        dbConfig = {
          host : configuration.host,
          dialect : 'mssql'
        };
        sequelize = new Sequelize(configuration.dbName, configuration.user, configuration.password, dbConfig);
      }
      db = sequelize;
      cb(null, sequelize);

    }

    var cwd = process.cwd();

    fs.readFile(cwd + '/aserver/config/database.json', 'utf8', (err, data) => {

      if (err) {

        if (err.code === 'ENOENT') {

          var defaultConfig = require('../config/database-default.js');

          fs.writeFile(cwd + '/aserver/config/database.json', JSON.stringify(defaultConfig), (configErr) => {
            if (configErr) {
              done(configErr, null);
              console.log('config write err', configErr);
            } else {
              sequel(defaultConfig, done);
            }
          });

        } else {
          done(err, null);
        }

      } else {

        let parsed = null;

        try {
          parsed = JSON.parse(data);
        } catch (e) {
          done(e, null);
        }

        sequel(parsed, done);

      }

    });

  }

}

module.exports = DatabaseConnection;