'use strict';

var
  express = require('express'),
  ZonesController = require('../controller/zones-controller'),
  router = express.Router();

router
  .get('/zones', (req, res) => {

    var criteria = null;

    if (req.query.name) {
      criteria = {
        where : {
          name : {
            $like : '%' + req.query.name + '%'
          }
        }
      };
    } else {
      criteria = {};
    }

    ZonesController.getAll(criteria)
      .then(zones => {
        res.json(zones);
      })
      .catch(err => {
        res.status(500).json(err);
      });

  })
  .get('/zones/:id', (req, res) => {
    ZonesController.getById(req.params.id)
      .then(zone => {
        res.json(zone);
      })
      .catch(err => {
        res.status(500).json(err);
      });
  })
  .post('/zones', (req, res) => {

    ZonesController.createZone(req.body)
      .then(zone => {
        res.json(zone);
      })
      .catch(err => {
        res.status(500).json(err);
      });

  })
  .post('/zones/:id', (req, res) => {

    ZonesController.addCoordinates(req.body.zoneId, req.body.lat, req.body.lng)
      .then(zone => {
        res.json(zone);
      })
      .catch(err => {
        res.status(500).json(err);
      });

  })
  .put('/zones/:id', (req, res) => {

    ZonesController.updateById(req.params.id, req.body)
      .then(rows => {
        ZonesController.getById(req.params.id)
          .then(zone => {
            res.json(zone);
          })
          .catch(err => {
            res.status(500).json(err);
          });
      })
      .catch(err => {
        res.status(500).json(err);
      });

  })
  .delete('/zones/:id', (req, res) => {
    ZonesController.deleteZone(req.params.id)
      .then(rows => {
        res.json(rows);
      })
      .catch(err => {
        res.status(500).json(err);
      });
  });

module.exports = router;