'use strict';

var
  express = require('express'),
  MobilesController = require('../controller/mobiles-controller'),
  router = express.Router();

router
  .get('/mobiles', (req, res) => {

    var criteria = null;

    if (req.query.name) {
      criteria = {
        where : {
          name : {
            $like : '%' + req.query.name + '%'
          }
        }
      };
    } else {
      criteria = {};
    }

    MobilesController.getAll(criteria)
      .then(mobiles => {
        res.json(mobiles);
      })
      .catch(err => {
        res.status(500).json(err);
      });

  })
  .get('/mobiles/:id', (req, res) => {
    MobilesController.getById(req.params.id)
      .then(mobile => {
        res.json(mobile);
      })
      .catch(err => {
        res.status(500).json(err);
      });
  })
  .post('/mobiles', (req, res) => {

    MobilesController.createMobile(req.body)
      .then(mobile => {
        res.json(mobile);
      })
      .catch(err => {
        res.status(500).json(err);
      });

  })
  .post('/mobiles/:id', (req, res) => {

    console.log('post');

    MobilesController.addCoordinates(req.body.mobileId, req.body.lat, req.body.lng)
      .then(mobile => {
        res.json(mobile);
      })
      .catch(err => {
        res.status(500).json(err);
      });

  })
  .put('/mobiles/:id', (req, res) => {

    MobilesController.updateById(req.params.id, req.body)
      .then(rows => {
        MobilesController.getById(req.params.id)
          .then(mobile => {
            res.json(mobile);
          })
          .catch(err => {
            res.status(500).json(err);
          });
      })
      .catch(err => {
        res.status(500).json(err);
      });

  })
  .delete('/mobiles/:id', (req, res) => {
    MobilesController.deleteMobile(req.params.id)
      .then(rows => {
        res.json(rows);
      })
      .catch(err => {
        res.status(500).json(err);
      });
  });

module.exports = router;