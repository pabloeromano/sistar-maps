'use strict';

var
  Sequelize = require('sequelize'),
  sequelize = require('../database/database-connection').getInstance(),
  Zone = require('./zone'),
  ZoneCoordinate = sequelize.define('ZoneCoordinate', {
    lat : Sequelize.STRING,
    lng : Sequelize.STRING
  }, {
    timestamps : true,
    tableName : 'ZoneCoordinate'
  });

ZoneCoordinate.belongsTo(Zone, { as : 'zone' });

module.exports = ZoneCoordinate;