'use strict';

var
  Sequelize = require('sequelize'),
  sequelize = require('../database/database-connection').getInstance(),
  Zone = sequelize.define('Zone', {
    name : Sequelize.STRING,
    visibility : Sequelize.BOOLEAN
  }, {
    timestamps : true,
    tableName : 'Zone'
  });

module.exports = Zone;