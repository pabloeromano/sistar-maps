'use strict';

var
  promiseSequence = require('util-promise').executeSequence,
  sequelize = require('../database/database-connection').getInstance(),
  Zone = require('./zone'),
  ZoneCoordinates = require('./zone-coordinate'),
  Mobile = require('./mobile'),
  MobileCoordinates = require('./mobile-coordinate');

function init () {

  var
    values = [],
    models = [
      Zone,
      ZoneCoordinates,
      Mobile,
      MobileCoordinates
    ];
  
  function seedZone () {
    return new Promise((resolve, reject) => {
      Zone.count()
        .then(count => {
          if (count === 0) {
            Zone.bulkCreate([
              {
                name : 'Zona 1',
                visibility : true
              },
              {
                name : 'Zona 2',
                visibility : true
              }
            ])
              .then(data => {
                resolve(data);
              })
              .catch(err => {
                reject(err);
              });
          } else {
            reject(null);
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  function seedMobile () {
    return new Promise((resolve, reject) => {
      Mobile.count()
        .then(count => {
          if (count === 0) {
            Mobile.bulkCreate([
              {
                name : 'Movil 1',
                visibility : true
              },
              {
                name : 'Movil 2',
                visibility : true
              },
              {
                name : 'Movil 3',
                visibility : true
              },
              {
                name : 'Movil 4',
                visibility : true
              }
            ])
              .then(data => {
                resolve(data);
              })
              .catch(err => {
                reject(err);
              });
          } else {
            reject(null);
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  function seedZoneCoordinate () {
    return new Promise((resolve, reject) => {
      ZoneCoordinates.count()
        .then(count => {
          if (count === 0) {
            ZoneCoordinates.bulkCreate([
              {
                zoneId : 2,
                lat : -27.473135,
                lng : -58.821863
              },
              {
                zoneId : 2,
                lat : -27.473092,
                lng : -58.823039
              },
              {
                zoneId : 2,
                lat : -27.473990,
                lng : -58.822261
              },
              {
                zoneId : 1,
                lat : -27.474142,
                lng : -58.823393
              },
              {
                zoneId : 1,
                lat : -27.473447,
                lng : -58.822840
              },
              {
                zoneId : 1,
                lat : -27.473037,
                lng : -58.821676
              },
              {
                zoneId : 1,
                lat : -27.474756,
                lng : -58.821145
              }
            ])
              .then(data => {
                resolve(data);
              })
              .catch(err => {
                reject(err);
              });
          } else {
            reject(null);
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  function seedMobileCoordinate () {
    return new Promise((resolve, reject) => {
      MobileCoordinates.count()
        .then(count => {
          if (count === 0) {
            MobileCoordinates.bulkCreate([
              {
                mobileId : 4,
                lat : -27.473547,
                lng : -58.822101
              },
              {
                mobileId : 4,
                lat : -27.473452,
                lng : -58.822906
              },
              {
                mobileId : 4,
                lat : -27.473414,
                lng : -58.823329
              },
              {
                mobileId : 1,
                lat : -27.474777,
                lng : -58.820901
              },
              {
                mobileId : 1,
                lat : -27.474275,
                lng : -58.820830
              },
              {
                mobileId : 1,
                lat : -27.473685,
                lng : -58.820744
              },
              {
                mobileId : 1,
                lat : -27.473609,
                lng : -58.821409
              },
              {
                mobileId : 2,
                lat : -27.472333,
                lng : -58.822289
              },
              {
                mobileId : 2,
                lat : -27.472485,
                lng : -58.820723
              },
              {
                mobileId : 2,
                lat : -27.472542,
                lng : -58.820025
              },
              {
                mobileId : 3,
                lat : -27.471610,
                lng : -58.821849
              },
              {
                mobileId : 3,
                lat : -27.472209,
                lng : -58.821924
              },
              {
                mobileId : 3,
                lat : -27.472990,
                lng : -58.821956
              },
              {
                mobileId : 3,
                lat : -27.473628,
                lng : -58.822021
              }
            ])
              .then(data => {
                resolve(data);
              })
              .catch(err => {
                reject(err);
              });
          } else {
            reject(null);
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  return new Promise((resolve, reject) => {
    promiseSequence(models, values, 'sync')
      .then(data => {
        Promise.all([
          seedMobile(),
          seedMobileCoordinate(),
          seedZone(),
          seedZoneCoordinate()
        ]);
      })
      .catch(err => {
        reject(err);
      });
  });

}

module.exports = init;