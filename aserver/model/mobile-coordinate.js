'use strict';

var
  Sequelize = require('sequelize'),
  sequelize = require('../database/database-connection').getInstance(),
  Mobile = require('./mobile'),
  MobileCoordinate = sequelize.define('MobileCoordinate', {
    lat : Sequelize.STRING,
    lng : Sequelize.STRING
  }, {
    timestamps : true,
    tableName : 'MobileCoordinate'
  });

MobileCoordinate.belongsTo(Mobile, { as : 'mobile' });

module.exports = MobileCoordinate;