'use strict';

var
  Sequelize = require('sequelize'),
  sequelize = require('../database/database-connection').getInstance(),
  Mobile = sequelize.define('Mobile', {
    name : Sequelize.STRING,
    visibility : Sequelize.BOOLEAN
  }, {
    timestamps : true,
    tableName : 'Mobile'
  });

module.exports = Mobile;