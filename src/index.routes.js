'use strict';

import React, { Component } from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app';
import Index from './components/index-route';
import SarGoogleMaps from './components/sar-google-map' 

function requireAuth (nextState, replace) {
  if (!sessionStorage.getItem('auth')) {
    replace({
      pathname : '/',
      state : { nextPathname : nextState.location.pathname }
    });
  }
}

export default(
  <Route path="/" component={App}>
    <IndexRoute component={Index}/>
    <Route path="sar-google-maps" component={SarGoogleMaps} onEnter={requireAuth}/>
  </Route>
);