'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

require('jquery');
require('../node_modules/bootstrap/dist/js/bootstrap');
require('../node_modules/bootstrap/dist/css/bootstrap.css');
require('../node_modules/font-awesome/css/font-awesome.css');

require('./styles.styl');

import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerMiddleware, routerReducer } from 'react-router-redux';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import { reducer as formReducer } from 'redux-form';

import Routes from './index.routes';
import reducers from './reducers';

const
  logger = createLogger({
    level : 'info',
    collapsed : true,
    duration : true
  }),
  reduxRouterMiddleware = routerMiddleware(browserHistory),
  createStoreWithMiddleware = applyMiddleware(thunk, reduxRouterMiddleware, logger)(createStore),
  store = createStoreWithMiddleware(
    combineReducers({
      ...reducers,
      routing : routerReducer,
      form : formReducer
    })
  ),
  history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history} routes={Routes}/>
  </Provider>,
  document.getElementById('app')
);