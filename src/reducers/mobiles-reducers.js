'use strict';

import {
  SELECT_MOBILE
} from '../actions/mobiles-actions';

const INITIAL_STATE = {
  fetching : false,
  user : null,
  token : null,
  error : null
};

function mobileReducer (state = INITIAL_STATE, action) {

  if (!action) {
    return state;
  }

  switch (action.type) {

    case SELECT_MOBILE :
      return {
        ...state,
        error : null
      };
      break;

    default :
      return state;
  }

}

export default mobileReducer;