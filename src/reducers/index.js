'use strict';

import zonesReducers from './zones-reducers';
import mobilesReducers from './mobiles-reducers';

const combinedReducers = {
  zones : zonesReducers,
  mobiles : mobilesReducers
};

export default combinedReducers;