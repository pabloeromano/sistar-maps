'use strict';

const
  INITIAL_STATE = {};

function indexReducer (state = INITIAL_STATE, action) {

  if (!action) {
    return state;
  }

  switch (action.type) {
    default :
      return state;
  }

}

export default indexReducer;