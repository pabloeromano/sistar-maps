'use strict';

import {
  SELECT_ZONE
} from '../actions/zones-actions';

const INITIAL_STATE = {
  fetching : false,
  user : null,
  token : null,
  error : null
};

function zoneReducer (state = INITIAL_STATE, action) {

  if (!action) {
    return state;
  }

  switch (action.type) {

    case SELECT_ZONE :
      return {
        ...state,
        error : null
      };
      break;

    default :
      return state;
  }

}

export default zoneReducer;