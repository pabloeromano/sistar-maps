'use strict';

import Overlay from './overlay';

class Zone extends Overlay {
  constructor (id, name, path, visibility) {
    super(id, name, path, visibility);
  }
  setPolygon (polygon) {
    this.polygon = polygon
  }

  toggleVisibility () {
    this.visibility = !this.visibility;
    if (this.polygon) {
      this.polygon.setVisible(this.visibility);
    }
  }
}

export default Zone