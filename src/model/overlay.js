'use strict';

class Overlay {
  constructor (id, name, path, visibility) {
    this.id = id;
    this.name = name;
    this.path = path;
    this.visibility = visibility;
  }

  toggleVisibility () {
    throw new Error('Not implemented')
  }
}

export default Overlay;