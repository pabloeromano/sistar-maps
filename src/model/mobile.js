'use strict';

import Overlay from './overlay';

class Mobile extends Overlay {
  constructor (id, name, path, visibility) {
    super(id, name, path, visibility);
    this.markers = [];
  }
  setPolyline (polyline) {
    this.polyline = polyline
  }

  toggleVisibility () {
    this.visibility = !this.visibility;
    if (this.polyline) {
      this.polyline.setVisible(this.visibility);
      this.markers.map(marker => {
        marker.setVisible(this.visibility);
      })
    }
  }
}

export default Mobile