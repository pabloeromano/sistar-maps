'use strict';

import axios from 'axios';

export const
  SELECT_MOBILE = 'SELECT_MOBILE',
  axiosInstance = axios.create();

axiosInstance.interceptors.request.use(
  function interceptor (config) {

    var token = JSON.parse(sessionStorage.auth);

    if (token) {
      config.headers['x-access-token'] = token;
    }

    return config;
  }
);

export function selectMobile (mobile) {
  return {
    type : SELECT_MOBILE,
    payload : {
      mobile
    }
  };
}