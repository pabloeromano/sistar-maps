'use strict';

import axios from 'axios';

export const
  SELECT_ZONE = 'SELECT_ZONE',
  axiosInstance = axios.create();

axiosInstance.interceptors.request.use(
  function interceptor (config) {

    var token = JSON.parse(sessionStorage.auth);

    if (token) {
      config.headers['x-access-token'] = token;
    }

    return config;
  }
);

export function selectMobile (zone) {
  return {
    type : SELECT_ZONE,
    payload : {
      zone
    }
  };
}