'use strict';

import React, {
  Component
} from 'react';
import { Button } from 'react-bootstrap'
import GoogleMaps from './containers/google-maps';
import moment from 'moment';
import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
import 'react-widgets/lib/less/react-widgets.less'
import Mobile from '../model/mobile'
import Zone from '../model/zone'

momentLocalizer(moment);

var styles = {
  mapContainer : {
    flex : 1,
    display : 'flex',
    alignItems : 'left',
    margin : 10
  },
  menuContainer : {
    flex : 1,
    display : 'flex',
    alignItems : 'right'
  },
  mapPanel : {
    display : 'flex',
    justifyContent : 'flex-end',
    backgroundColor : '#c3c3c3',
    padding : 8
  }
};

class SarGoogleMap extends Component {

  constructor (props) {
    super(props);

    this.state = {
      m: moment(),
      mapOpt :
      {
        id : '#map',
        lat : -27.473744,
        lng : -58.820870,
        width : '1000px',
        height : '500px',
        zoom : 17
      },
      mobiles : [
        {
          id : 1,
          name : 'Movil 1',
          path : [[-27.474777, -58.820901],[-27.474275, -58.820830],[-27.473685, -58.820744]],
          visibility : true
        },
        {
          id : 2,
          name : 'Movil 2',
          path : [[-27.473547, -58.822101],[-27.473452, -58.822906],[-27.473414, -58.823329],[-27.472843, -58.823292]],
          visibility : true
        }
      ],
      zones : [
        {
          id : 1,
          name : 'Zona 1',
          path : [[-27.473135, -58.821863],[-27.473092, -58.823039],[-27.473990, -58.822261]],
          visibility : true
        }
      ]
    }
  }

  componentWillMount () {
    this.setState({
      mobiles: this.state.mobiles.map(mobile => {
        return new Mobile(mobile.id, mobile.name, mobile.path, mobile.visibility);
      }),
      zones: this.state.zones.map(zone => {
        return new Zone(zone.id, zone.name, zone.path, zone.visibility);
      })
    });
  }
  
  handleChange(m) {
    this.setState({m: m});
  }

  createButton (elements) {
    return elements.map((element, index) => {
      return (
        <div key={index}>
          <Button
            className="btn btn-group btn-primary btn-sm"
            onClick={element.toggleVisibility.bind(element)}>
            {element.name}
          </Button>
        </div>
      );
    })
  }

  render () {

    return (
      <div style={styles.mapPanel}>
          <div style={styles.mapContainer}>
            <GoogleMaps
              mapOpt={this.state.mapOpt}
              mobiles={this.state.mobiles}
              zones={this.state.zones}
            />
          </div>
          <h4>Menu de Moviles</h4>
          {this.createButton(this.state.mobiles, 'mobiles')}

        <div style={styles.menuContainer}>d
          <h4>Menu de Zonas</h4>
          {this.createButton(this.state.zones, 'zones')}
        </div>
        <div>
          <DateTimePicker
            culture="es"
            defaultValue={new Date()}
            format={"DD/MM/YYYY, H:mm"}
          /><br/>
          <DateTimePicker
            culture="es"
            defaultValue={new Date()}
            format={"DD/MM/YYYY, H:mm"}
          /><br/>
          <input placeholder="Filtro de mobiles"/><br/>
          <input placeholder="Filtro de zonas"/>
        </div>
      </div>
    );
  }
}

export default SarGoogleMap;