'use strict';

import React, {
  Component
} from 'react';

import Map from './sar-google-map'

var styles = {
  container : {
    display : 'flex',
    flexGrow : 1,
    flexDirection : 'row',
    backgroundColor : '#c3c3c3'
  }
};

class IndexRoute extends Component {

  constructor (props) {
    super(props);
  }

  render () {
    return (
      <div style={styles.container}>
        <Map></Map>
      </div>
    );
  }
}

export default IndexRoute;