'use strict';

import React, {
  Component,
  PropTypes
} from 'react';
import GMaps from 'gmaps';
import _ from 'lodash';
import GoogleMapsLoader from '../../helpers/google-maps-loader';


function randomColorGenerator () {
  let
    chars = '0123456789ABCDEF'.split(''),
    hexColor = '#';
  for (let i = 0; i < 6; i++ ) {
    hexColor += chars[Math.floor(Math.random() * 16)];
  }

  return hexColor;
}


class GoogleMaps extends Component {

  componentDidMount () {
    let drawingColor = randomColorGenerator();
    
    GoogleMapsLoader.load((google) => {
      var map = new GMaps({
        el : '#map',
        lat : this.props.mapOpt.lat,
        lng : this.props.mapOpt.lng,
        width : this.props.mapOpt.width,
        height : this.props.mapOpt.height,
        zoom : this.props.mapOpt.zoom
      });

      var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode : google.maps.drawing.OverlayType.POLYGON,
        drawingControl : true,
        drawingControlOptions : {
          position : google.maps.ControlPosition.TOP_CENTER,
          drawingModes : [
            google.maps.drawing.OverlayType.POLYGON
          ]
        },
        polygonOptions : {
          fillColor : drawingColor,
          fillOpacity : 0.5,
          strokeColor : drawingColor,
          strokeWeight : 2,
          clickable : false
        }
      });
      drawingManager.setMap(map.map);

      this.props.mobiles.map((mobile) => {
        if (mobile.visibility) {
          map.drawPolyline({
            path : mobile.path,
            strokeColor : randomColorGenerator(),
            strokeOpacity : 0.6,
            strokeWeight : 6,
            icons : [{
              icon : {
                path : google.maps.SymbolPath.FORWARD_OPEN_ARROW
              },
              offset : '25px',
              repeat : '70px'
            }]
          });
          mobile.setPolyline(_.last(map.polylines));
          mobile.path.map((coordinates) => {
            map.addMarker({
              lat : coordinates[0],
              lng : coordinates[1]
            });
            mobile.markers.push(_.last(map.markers))
          });
        }
      });
      
      this.props.zones.map((zone) => {
        let color = randomColorGenerator();
        if (zone.visibility) {
          map.drawPolygon({
            paths : zone.path,
            strokeColor :color,
            strokeOpacity : 1,
            strokeWeight : 3,
            fillColor : color,
            fillOpacity : 0.6
          });
          zone.setPolygon(_.last(map.polygons))
        }
      });
    })
  }

  render () {
    return (
      <div className="map-holder">
        <div id="map"></div>
      </div>
    );
  }
}

GoogleMaps.propTypes = {
  mapOpt : PropTypes.object,
  mobiles : PropTypes.array,
  zones : PropTypes.array
};

GoogleMaps.defaultProps = {
  mapOpt : {},
  mobiles : [],
  zones : []
};

export default GoogleMaps;