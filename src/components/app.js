'use strict';

import React, {
  Component,
} from 'react';

import { Router, Link } from 'react-router';

class App extends Component {

  constructor (props) {
    super(props);
  }

  isActiveRoute (routePath) {
    if (this.props.location.pathname === routePath) {
      return 'active';
    } else {
      return '';
    }
  }

  render () {
    return (
      <div>
        <h4>App</h4>
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default App;

