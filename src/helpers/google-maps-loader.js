'use strict';

import GoogleMapsLoader from 'google-maps';

GoogleMapsLoader.KEY = 'AIzaSyDx1jr-6SATPSDLqt4_v9HrMkYJIghAi1g';
GoogleMapsLoader.VERSION = '3.24.10';
GoogleMapsLoader.LIBRARIES = [ 'geometry', 'drawing' ];

export default GoogleMapsLoader;