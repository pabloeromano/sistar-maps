'use strict';

const
  path = require('path'),
  webpack = require('webpack');

var config = {

  context : path.resolve(__dirname, 'src'),

  entry : [
    './index.js'
  ],

  output : {
    path : __dirname + '/public',
    filename : 'index.bundle.js'
  },

  plugins : [
    new webpack.ProvidePlugin({
      $ : "jquery",
      jQuery : "jquery"
    })
  ],

  module : {
    loaders : [
      {
        test : /\.jsx?$/,
        exclude : /node_modules/,
        loader : 'babel',
        query : {
          presets : [ 'es2015', 'react', 'stage-2' ]
        }
      },
      { test : /\.css$/, loader : 'style!css' },
      { test : /\.styl$/, loader : 'style!css!stylus' },
      { test : /\.less/, loader : 'style-loader!css-loader!less-loader' },
      {
        test : /\.(otf|eot|png|svg|ttf|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader : 'url?limit=8192'
      },
      {
        test : /\.jpe?g$|\.bmp$|\.gif$|\.png$/i,
        loader : "url"
      }
    ]
  }
};

// source maps for code inspection in the browser

config.devtool = 'source-map';

if (process.env.NODE_ENV === 'production') {
  config.plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = config;