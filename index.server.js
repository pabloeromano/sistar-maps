'use strict';

var
  path = require('path'),
  express = require('express'),
  http = require('http'),
  async  = require('async'),
  fs = require('fs'),
  favicon = require('serve-favicon'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
// Server
  app = express(),
  server = http.Server(app),
// Databse Connection
  DatabaseConnection = require('./aserver/database/database-connection');

/**
 * Models
 */

DatabaseConnection.connect((err, connection) => {

  function startServer () {
    server.listen(8080, () => {
      console.log('listening 8080');
    });
  }

  if (err) {
    console.log('db connection err ', err);
  } else {
    var
      models = require('./aserver/model/init');

    models()
      .then(relations => {
        console.log('base model init done', relations);
      })
      .catch(connectionErr => {
        if (connectionErr === null) {
          console.log('err null, init done, not need to seed');
        } else {
          console.log('base model init err', connectionErr);
        }
      });

    let
    // Routers
      routerMobile = require('./aserver/routes/mobile'),
      routerZone = require('./aserver/routes/zone'),
    // CWD
      cwd = process.cwd();

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended : false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));
    
    app.use('/api', routerMobile);
    app.use('/api', routerZone);

    app.all('/*', (req, res) => {
      // Just send the index.html for other files to support HTML5Mode
      res.sendFile(path.join(__dirname + '/public/index.html'));
    });

    fs.readFile(cwd + '/config.json', 'utf8', (serverErr, data) => {

      if (serverErr) {

        if (serverErr.code === 'ENOENT') {

          var defaultConfig = require('./aserver/config/database-default.js');

          fs.writeFile(cwd + '/config.json', JSON.stringify(defaultConfig), (configErr) => {
            if (configErr) {
              console.log('config write err', configErr);
            } else {
              startServer();
            }
          });
        } else {
          console.log('some other err', serverErr);
        }
      } else {
        startServer();
      }

    });
  }
});